import merge from 'lodash/merge'
import { PubSub } from 'apollo-server-express'
import { GraphQLScalarType } from 'graphql'
import passport from 'passport'
import auth from './auth'
import user from './user'

import gqlLoader from './gqlLoader'

require('./../passportHelper')(passport)

const pubsub = new PubSub()

const resolverMap = {
  DateTime: new GraphQLScalarType({
    name: 'DateTime',
    description: 'A date and time, represented as an ISO-8601 string',
    serialize: value => value.toISOString(),
    parseValue: value => new Date(value),
    parseLiteral: ast => new Date(ast.value)
  })
}

const serverConfig = {
  introspection: true,
  uploads: false,
  playground: true,
  tracing: true,
  typeDefs: [
    gqlLoader('./index.graphql'),
    user.typeDefs,
    auth.typeDefs
  ].join(' '),
  resolvers: merge(
    {},
    auth.resolvers,
    user.resolvers,
    resolverMap
  ),
  context: ({ req, connection }) => {
    return {
      user: connection ? null : req.user,
      logout: connection ? function emptyFunction() {} : req.logout,
      loaders: {
        userLoader: user.loader
      },
      models: {
        User: user.model
      },
      pubsub
    }
  }
}
module.exports = serverConfig
