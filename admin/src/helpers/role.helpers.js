import Employee from '../img/employee.svg'
import HeadOfDepartment from '../img/headOfDepartment.svg'
import Head from '../img/head.svg'
import HRManager from '../img/hrManager.svg'
import Accountant from '../img/accountant.svg'

export const getAvatarByRole = ({ role }) => {
	switch (role) {
		case 'employee':
			return Employee
		case 'headOfDepartment':
			return HeadOfDepartment
		case 'head':
			return Head
		case 'hrManager':
			return HRManager
		case 'accountant':
			return Accountant
		default:
			return HeadOfDepartment
	}
}

export const getNameByRole = ({ role }) => {
	switch (role) {
		case 'employee':
			return 'Сотрудник'
		case 'headOfDepartment':
			return 'Начальник отдела'
		case 'head':
			return 'Руководитель организаций'
		case 'hrManager':
			return 'Начальник HR'
		case 'accountant':
			return 'Бухгалтер'
		default:
			return 'Начальник отдела'
	}
}