import React from 'react'
import PropTypes from 'prop-types'
import { Link, useHistory } from 'react-router-dom'
import {
  LogoutOutlined,
  TeamOutlined,
  RestOutlined,
  FileProtectOutlined 
} from '@ant-design/icons'
import { Menu } from 'antd'
import { useAuth } from '../context/useAuth'

import Loading from '../pages/shared/Loading'
import styled from 'styled-components'

const { SubMenu } = Menu;


const StyledMenu = styled(Menu)`
  border-bottom-right-radius: 8px;
  border-top-right-radius: 8px;
  min-height: 100vh;
`
function MainMenu({ currentUrl }) {
  const { user, logout } = useAuth()
  const history = useHistory()

  const headOfDepartment = [
    {  name: 'Командировка', icon: FileProtectOutlined, items: [
      {
        link: '/headOfDepartmentBuisnessProcess',
        name: 'Бизнес процесс'
      },
      {
        link: '/headOfDepartmentArchive',
        name: 'Архив'
      }
    ]}
  ]

  const employee = [
    {  name: 'Командировка', icon: FileProtectOutlined, items: [
      {
        link: '/employeeBuisnessTrip',
        name: 'Увидомнеие'
      },
      {
        link: '/employeeArchive',
        name: 'Архив'
      }
    ]}
  ]

  const head = [
    {  name: 'Командировка', icon: FileProtectOutlined, items: [
      {
        link: '/headBuisnessTrip',
        name: 'Увидомнеие'
      },
      {
        link: '/headArchive',
        name: 'Архив'
      }
    ]}
  ]

  const accountant = [
    {  name: 'Командировка', icon: FileProtectOutlined, items: [
      {
        link: '/accountantOrders',
        name: 'Увидомнеие'
      },
      {
        link: '/accountantArchive',
        name: 'Архив'
      }
    ]}
  ]
  const hrManager = [
    {  name: 'Командировка', icon: FileProtectOutlined, items: [
      {
        link: '/hrManagerOrders',
        name: 'Увидомнеие'
      },
      {
        link: '/hrManagerArchive',
        name: 'Архив'
      }
    ]}
  ]

  const userToMenu = {
    headOfDepartment: headOfDepartment,
    employee: employee,
    head: head,
    hrManager: hrManager,
    accountant: accountant
  }

  const currentMenu = userToMenu[user?.role] || []

  const onLogoutClick = () => {
    logout()
    history.push('/login')
  }
  if(!user) {
    return <Loading />
  }
  if (user) {
    return (
      <StyledMenu selectedKeys={[currentUrl]} mode="inline" theme="light">
        {currentMenu.map(menu => (
          <SubMenu key="sub1" icon={<menu.icon />} title={menu.name}>
            {menu?.items?.length > 0 && menu?.items?.map(item => (
              <Menu.Item key={item.link}>
                <Link to={item.link} key={item.link}>{item.name}</Link>
              </Menu.Item>
            ))}
        </SubMenu>
        ))}
      </StyledMenu>
    )
  }
  return <React.Fragment />
}

MainMenu.propTypes = {
  currentUrl: PropTypes.string.isRequired
}

export default MainMenu
