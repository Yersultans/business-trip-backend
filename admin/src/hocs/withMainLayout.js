import React from 'react'
import styled from 'styled-components'
import { Layout, Menu, Dropdown, Button } from 'antd'
import { Link, useHistory } from 'react-router-dom'
import { LogoutOutlined } from '@ant-design/icons'

import MainMenu from './MainMenu'
import Logo from '../pages/logo.svg'
import Icon1 from '../img/icon1.svg'
import Icon2 from '../img/icon2.svg'
import { getAvatarByRole, getNameByRole } from '../helpers/role.helpers'
import { useAuth } from '../context/useAuth'

const { Header, Content, Sider } = Layout


const StyledHeader = styled(Header)`
  min-height: 120px;
  background-color: #f0f2f5;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 25px;
`

const StyledLayout = styled(Layout)`
  min-height: 100vh;
`
const MainLayout = styled(Layout)`
  overflow-y: scroll;
  min-width: 1110px;
`

const StyledSider = styled(Sider)`
  background-color: #f0f2f5;
  padding-right: 16px;
`

const StyledContent = styled(Content)`
  background-color: #fff;
  min-height: 500px;
  min-width: 700px;
  border-radius: 8px;
  margin-right: 16px;
  margin-left: 16px;
  padding: 32px;

`
const LogoContainer = styled.div`
  display: flex;
  box-sizing: border-box;
  background: #fff;
  background-color: #f0f2f5;
  align-items: center;
  padding: 24px 16px 16px 24px;
`
const LogoText = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 20px;
`

const UserContainer = styled.div`
  display: flex;
  align-items: center;
  box-sizing: border-box;
`

const MenuItem = styled.div`
  margin-left: 25px;
  max-width: 40px;
`

const UserName = styled.div`
  margin-left: 25px;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 35px;
`

const withMainLayout = Page => {
  
  return props => {
    const { user, fetchUser, logout } = useAuth()
    const history = useHistory()
    const currentUrl = history.location.pathname

    const [currentHeader, setCurrentHeader] = React.useState('')

    const adminMenu = [
      { link: '/users', name: 'Пользователи' }
    ]
    const onLogoutClick = () => {
      logout()
      history.push('/home')
    }

    const menu = (
      <Menu>
        <Menu.Item
          key="username"
        >
         {user ? `${user.lastname} ${user.firstname}`  : 'Начальник Отдела'}
        </Menu.Item>
        <Menu.Item
          key="logout"
          icon={<LogoutOutlined />}
          onClick={onLogoutClick}
          danger
        >
          Выйти
        </Menu.Item>
      </Menu>
    )

    React.useEffect(() => {
      adminMenu.forEach(item => {
        if (currentUrl.startsWith(item.link)) {
          setCurrentHeader(item.name)
        }
      })
    }, [])
    return (
      <MainLayout>
        <StyledHeader>
          <LogoContainer>
            <img src={Logo} alt="logo" />
            <LogoText> - сервис управления командировочным заданием</LogoText>
          </LogoContainer>
          <UserContainer>
            <MenuItem>
              <img src={Icon1} alt="icon1" />
            </MenuItem>
            <Dropdown overlay={menu}>
              <MenuItem onClick={() => {
							  history.push('/headOfDepartmentBuisnessProcess')
							  }}>
								  <img src={getAvatarByRole({ role: user?.role })} alt="logo" />
						  </MenuItem>
            </Dropdown>
            
            <UserName>
              {getNameByRole({ role: user?.role })}
            </UserName>
          </UserContainer>
        </StyledHeader>
      <StyledLayout style={{
        marginBottom: 64
      }}>
        <StyledSider width={300}>
          <MainMenu {...{ currentUrl, history, setCurrentHeader }} />
        </StyledSider>
        <StyledContent>
          <Page {...props} />
        </StyledContent>
      </StyledLayout>
    </MainLayout>
    )
  }
}

export default withMainLayout
