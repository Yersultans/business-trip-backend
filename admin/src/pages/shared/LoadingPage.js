import React from 'react'
import styled from 'styled-components'
import Loading from './Loading'

const MainContainer = styled.div`
  width: 100vw;
  height: 100vh;
`

function LoadingPage() {
  return (
    <MainContainer>
      <Loading />
    </MainContainer>
  )
}

export default LoadingPage
