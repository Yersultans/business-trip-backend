import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar, DatePicker } from 'antd'
import { CloseOutlined, CheckOutlined} from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'
import moment from 'moment'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditUser = ({
  visible,
  onCancel,
  title,
  editUser,
  onUpdate,
  onDelete,
  avatarUrl, 
  setAvatarUrl
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Создать"
      cancelText="Закрыть"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editUser.id)
            })
          }}
        >
          Удалить
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Отмена
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                delete values.avatarUrl
                onUpdate(editUser.id, { ...values, avatarUrl })
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Сохранить
        </Button>
      ]}
    >
      <Form
        form={form}
        layout="vertical"
      >
        <FormItem
            key="username"
            label={getTooltip('Email пользователя', 'Email пользователя')}
            name="username"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Email пользователя`
              }
            ]}
						initialValue={editUser.username}
          >
            <Input placeholder="Email" />
          </FormItem>
					<FormItem
            key="lastname"
            label={getTooltip('Фамилия пользователя', 'Фамилия пользователя')}
            name="lastname"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Фамилия пользователя`
              }
            ]}
						initialValue={editUser.lastname}
          >
            <Input placeholder="Фамилия" />
          </FormItem>
					<FormItem
            key="firstname"
            label={getTooltip('Имя пользователя', 'Имя пользователя')}
            name="firstname"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Имя пользователя`
              }
            ]}
						initialValue={editUser.firstname}
          >
            <Input placeholder="Имя" />
          </FormItem>
					<FormItem
            key="phoneNumber"
            label={getTooltip('Телефон заведения', 'Телефон заведения')}
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Телефон заведения`
              }
            ]}
						initialValue={editUser.phoneNumber}
          >
            <Input placeholder="Телефон" />
          </FormItem>
					<FormItem
            key="birthday"
            label={getTooltip('Дата рождения пользователя', 'Дата рождения пользователя')}
            name="birthday"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Дата рождения пользователя`
              }
            ]}
						initialValue={moment(editUser.birthday)}
          >
            <StyledDatePicker placeholder="Выберите дату" />
          </FormItem>
          <FormItem
            key="gender"
            label={getTooltip(
              'Пол',
              'Пол пользователя'
            )}
            name="gender"
            rules={[
              {
                required: true,
                message: `Пожалуйста, выберите Пол пользователя`
              }
            ]}
						initialValue={editUser.gender}
          >
            <Select mode="single" placeholder="выберите Пол" showSearch>
              <Select.Option key="male" value="male">
                Мужской
              </Select.Option>
              <Select.Option key="female" value="female">
                Женский
              </Select.Option>
            </Select>
          </FormItem>
					<FormItem
            key="role"
            label={getTooltip(
              'Роль',
              'Роль пользователя'
            )}
            name="role"
            rules={[
              {
                required: true,
                message: `Пожалуйста, выберите Роль пользователя`
              }
            ]}
						initialValue={editUser.role}
          >
            <Select mode="single" placeholder="выберите Роль" showSearch>
              <Select.Option key="admin" value="admin">
                Админ
              </Select.Option>
              <Select.Option key="user" value="user">
                Клиент
              </Select.Option>
              <Select.Option key="driver" value="driver">
                Водитель
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="avatarUrl"
            label={getTooltip('Аватарка', 'Аватарка пользователя')}
            name="avatarUrl"
            rules={[
              {
                required: false,
                message: `Пожалуйста, загрузите Аватарку`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setAvatarUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={avatarUrl || editUser.avatarUrl} />
            </ImageUploadContainer>
          </FormItem>
          <FormItem
            key="isBlocked"
            label={getTooltip('Блокнут?', 'Пользователь Блокнут?')}
            name="isBlocked"
            valuePropName="isBlocked"
          >
            <Switch 
              defaultChecked ={editUser?.isBlocked}
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          </FormItem>
      </Form>
    </Modal>
  )
}

EditUser.propTypes = {
  editUser: PropTypes.shape({

  }).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditUser
