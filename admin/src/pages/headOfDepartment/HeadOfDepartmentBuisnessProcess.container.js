import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { Form, Input, Button, Select, DatePicker } from 'antd'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import Modal from 'react-bootstrap/Modal'
import { UserOutlined, LockOutlined, PlusOutlined, MinusOutlined  } from '@ant-design/icons'
import 'moment/locale/ru';
import locale from 'antd/es/date-picker/locale/ru_RU';


import Loading from '../shared/Loading'
import { useAuth } from '../../context/useAuth'
import { removeAnnoyingHeader } from '../../utils/apollo'
import WithMainLayout from '../../hocs/withMainLayout'

const { RangePicker } = DatePicker

const Container = styled.div`

`

const TitleContent = styled.div`
  display: flex;
  align-items: center;
`

const Title = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 24px ;
`

const StyledForm = styled(Form)`
  max-width: 600px;
  margin-bottom: 32px;
`

const StyledFormItem = styled(Form.Item)`
  margin-top: 10px;
  margin-bottom: 10px;
`

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
}

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16
  }
}

const HeadOfDepartmentBuisnessProcessContainer = () => {
  const [isCreateForm, setIsCreateForm] = React.useState(false)

  return (
    <Container>
      <TitleContent>
        <Title>Бизнес процесс</Title>
      </TitleContent>
      <StyledForm
      {...layout}
      >
        <StyledFormItem
          labelAlign="left"
          label="Бизнес процесс"
          name="username"
        >
          <Select mode="single" placeholder="">
            <Select.Option key="male" value="male">
              Командирование
            </Select.Option>
          </Select>
        </StyledFormItem>
      </StyledForm>
      <TitleContent>
        <Title>Создать представление </Title>
        {isCreateForm ? (
          <MinusOutlined onClick={() => setIsCreateForm(false)}style={{ fontSize: 20, marginLeft: 12 }}/>
        ) : (
          <PlusOutlined onClick={() => setIsCreateForm(true)} style={{ fontSize: 20, marginLeft: 12 }}/>
        )}
        
      </TitleContent>
      {isCreateForm && (
        <StyledForm 
        {...layout}
        >
        <StyledFormItem
          label="Дата:"
          name="username"
          labelAlign="left"
          
        >
          <RangePicker  locale={locale}/>
        </StyledFormItem>
        <StyledFormItem
          label="Место командировки:"
          name="username"
          labelAlign="left"
        >
          <Select mode="single" placeholder="">
            <Select.Option key="male" value="male">
              Командирование
            </Select.Option>
          </Select>
        </StyledFormItem>
        <StyledFormItem
          label="Цель командировки"
          name="username"
          labelAlign="left"
        >
          <Select mode="single" placeholder="">
            <Select.Option key="male" value="male">
              Командирование
            </Select.Option>
          </Select>
        </StyledFormItem>
        <StyledFormItem
          label="ФИО сотрудника"
          name="username"
          labelAlign="left"
        >
          <Select mode="single" placeholder="">
            <Select.Option key="male" value="male">
              Командирование
            </Select.Option>
          </Select>
        </StyledFormItem>
        <StyledFormItem {...tailLayout}>
          <Button type="primary" htmlType="submit" size="large">
            Подписать
          </Button>
        </StyledFormItem>
      </StyledForm>
      )}
    </Container>
  )
}

HeadOfDepartmentBuisnessProcessContainer.propTypes = {
}

export default WithMainLayout(HeadOfDepartmentBuisnessProcessContainer)
