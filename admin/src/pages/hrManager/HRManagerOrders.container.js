import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { Form, Input, Button, Table } from 'antd'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import Modal from 'react-bootstrap/Modal'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import moment from 'moment'
import 'moment/locale/ru'


import Loading from '../shared/Loading'
import { useAuth } from '../../context/useAuth'
import { removeAnnoyingHeader } from '../../utils/apollo'
import WithMainLayout from '../../hocs/withMainLayout'

const Container = styled.div`

`

const TitleContent = styled.div`
  display: flex;
  align-items: center;
`

const Title = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 24px ;
`

const StyledTable = styled(Table)`
  margin-top: 32px;
`

const HRManagerOrdersContainer = () => {

  const data = [
    {
      number: '152',
      date: '2022-03-17T00:00:05.199Z',
      fullname: 'Иванов Иван Иванович'
    },
    {
      number: '165',
      date: '2022-03-25T00:00:05.199Z',
      fullname: 'Мария Вячаславна Струлуцкая'
    },
    {
      number: '152',
      date: '2022-03-25T00:00:05.199Z',
      fullname: 'Иван Демченко Строгий'
    }
  ]

  const columns = [
    {
      title: 'Заявка №',
      dataIndex: 'number',
      width: '25%'
    },
    {
      title: 'Дата',
      render: (_, item) => {
        return (
          <span> {moment(item?.date).format('DD.MM.YYYY')}</span>
        )
      },
      width: '25%'
    },
    {
      title: 'ФИО',
      dataIndex: 'fullname',
      width: '25%'
    }
  ]
  return (
    <Container>
      <TitleContent>
        <Title>Заявки на командирование</Title>
      </TitleContent>
      <StyledTable
        dataSource={data}
        columns={columns}
        rowKey={item => item.id}
      />
    </Container>
  )
}

HRManagerOrdersContainer.propTypes = {
}

export default WithMainLayout(HRManagerOrdersContainer)
