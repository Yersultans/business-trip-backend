import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { Form, Input, Button } from 'antd'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import Modal from 'react-bootstrap/Modal'
import { UserOutlined, LockOutlined } from '@ant-design/icons'


import Loading from '../shared/Loading'
import { useAuth } from '../../context/useAuth'
import { removeAnnoyingHeader } from '../../utils/apollo'
import WithMainLayout from '../../hocs/withMainLayout'

const EmployeeProcessContainer = () => {


  return (
    <div>Process</div>
  )
}

EmployeeProcessContainer.propTypes = {
}

export default WithMainLayout(EmployeeProcessContainer)
