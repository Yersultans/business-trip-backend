import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { Form, Input, Button } from 'antd'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import Modal from 'react-bootstrap/Modal'
import { UserOutlined, LockOutlined } from '@ant-design/icons'

import Loading from '../shared/Loading'
import Logo from '../logo.svg'
import Image1 from '../../img/image1.png'
import Image2 from '../../img/image2.png'
import Employee from '../../img/employee.svg'
import { getAvatarByRole } from '../../helpers/role.helpers'
import { useAuth } from '../../context/useAuth'
import { removeAnnoyingHeader } from '../../utils/apollo'



const LOGIN = gql`
  mutation loginUser($input: LoginUserInput) {
    loginUser(input: $input) {
      token
			user {
        id
        username
        role
      }
    }
  }
`
const tailLayout = {
  wrapperCol: {
    offset: 20,
    span: 16
  }
}
const SubmitButton = styled(Button)`
`
const russianText = {
  login: 'Логин',
  password: 'Пароль',
  submit: 'Войти'
}

const Container = styled.div`
	max-width: 1110px;
	margin: 0 auto!important;
`

const Header = styled.header`
  display: flex;
  justify-content: space-between;
	align-items: center;
`

const LogoContainer = styled.div`
  box-sizing: border-box;
  background: #fff;
  padding: 32px 0;
`

const Menu = styled.div`
	display: flex;
`
const MenuItem = styled.div`
	margin: 0 32px;
	
`
const Content = styled.div`
	margin-top: 164px;
	display: flex;
  justify-content: space-between;
`

const Info = styled.div`
	max-width: 670px;
	padding-left: 95px;
`

const Title = styled.div`
	font-style: normal;
	font-weight: 600;
	font-size: 36px;
	line-height: 50px;
`

const Description = styled.div`
	font-style: normal;
	font-weight: 200;
	font-size: 30px;
	line-height: 36px;
	color: #1F1F21;
	margin-top: 32px;
`

const ContentImg = styled.div`

`
const Content2 = styled.div`
	margin-top: 100px;
	display: grid;
`
const TitleCenter = styled.div`
	padding-left: 40px;
	font-style: normal;
	font-weight: 600;
	font-size: 36px;
	line-height: 50px;
	
`

const Row = styled.div`
	display: grid;
	grid-template-columns: repeat(3, 1fr);
	margin-top: 64px;

`
const Col1 = styled.div`
	padding: 48px;
	border-style: solid;
  border-width: 0.5px;
  border-color: #AFAFAF;
  border-top-left-radius: 30px;
	border-bottom-left-radius: 30px;
  border-right: 0;
`
const Col2 = styled.div`
	padding: 48px;
	border-style: solid;
  border-width: 0.5px;
  border-color: #AFAFAF;
`
const Col3 = styled.div`
	padding: 48px;	
	border-style: solid;
  border-width: 0.5px;
  border-color: #AFAFAF;
  border-top-right-radius: 30px;
	border-bottom-right-radius: 30px;
  border-left: 0;
` 

const Content3 = styled.div`
	margin-top: 50px;
	display: grid;
`
const DescriptionCenter = styled.div`
	font-style: normal;
	font-weight: 200;
	font-size: 30px;
	line-height: 36px;
	color: #1F1F21;
	margin-top: 32px;
	text-align: center;
`

const Title2 = styled.div`
	font-style: normal;
	font-weight: 600;
	font-size: 36px;
	line-height: 50px;
	margin-bottom: 64px;
`

const ColTilte = styled.div`
	font-style: normal;
	font-weight: 500;
	font-size: 24px;
	line-height: 29px;
	text-align: center;
`

const ColDesc = styled.div`
	font-style: normal;
	font-weight: 300;
	font-size: 20px;
	line-height: 23px;
	text-align: center;
	margin-top: 20px;
`

const SignInButton = styled.button`
	background-color: #FF5959;
	border: none;
	color: #F7F7F7;
	border-radius: 8px;
	padding: 8px 16px;
`

const layout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 16
  }
}

const HomeContainer = () => {
	const [modalVisible, setModalVisible] = React.useState(false)
	const history = useHistory()
  const [login, { data, error, loading }] = useMutation(LOGIN)
  const { user, fetchUser } = useAuth()

  const handleSubmit = values => {
    const { username, password } = values
    login({
      variables: {
        input: { username, password }
      },
      errorPolicy: 'all'
    })
  }

  const userToStartPage = {
    admin: '/users',
		headOfDepartment: '/headOfDepartmentBuisnessProcess',
		employee: '/employeeBuisnessTrip',
		head: '/headBuisnessTrip',
		accountant: '/accountantOrders',
		hrManager: '/hrManagerOrders'
	}

  useEffect(() => {
    if (!loading && error) {
      toast.error(removeAnnoyingHeader(error.message))
    } else if (data && data?.loginUser && data.loginUser?.token && data.loginUser.user && !loading) {
      localStorage.setItem('token', `${data.loginUser?.token}`)
			localStorage.setItem('role', `${data.loginUser?.user?.role}`)
      toast.success('Успешно вошли в систему')
      fetchUser()
      const startPage =
        userToStartPage[data.loginUser.user.role] || '/headOfDepartmentBuisnessProcess'
			console.log('startPage', startPage)
      history.push(startPage)
    }
  }, [data, loading, error])

	useEffect(() => {
    if (user) {
      const startPage = userToStartPage[user.role] || '/users'
      history.push(startPage)
    }
  }, [user])

  return (
		<div style={{ paddingBottom: 95}}>
		<Container>
			<Header>
				<LogoContainer>
            <img src={Logo} alt="logo" />
        </LogoContainer>
				<Menu>
					<MenuItem style={{
						borderBottomWidth: 1,
						borderBottomStyle: 'solid',
						borderBottomColor: 'black'
					}}
					>Инструкция</MenuItem>
					<MenuItem>Контакты</MenuItem>
				</Menu>
				<div>
					{user ? ( 
						<div onClick={() => {
							const startPage =
        				userToStartPage[user?.role] || '/headOfDepartmentBuisnessProcess'
								history.push(startPage)
							}}>
								<img src={getAvatarByRole({ role: user?.role })} alt="logo" />
						</div>
					) : (
						<SignInButton onClick={() => {
							setModalVisible(true)
							}}>Войти
						</SignInButton>
					)}
					
				</div>
			</Header>
			<Content>
					<Info>
						<TitleCenter>Автоматизация командировок</TitleCenter>
						<Description>Subway позволяет снизить трудозатраты на организацию командировок и оформление документов</Description>
					</Info>
					<ContentImg>
						<img src={Image1} alt="logo" />
					</ContentImg>
			</Content>
			<Content2>
					<TitleCenter>Возможности автоматизированной системы</TitleCenter>
					<Row>
						<Col1>
							<ColTilte>Возможность избежать ошибок</ColTilte>
							<ColDesc>При возникновении ошибок в организации командировок, которые приводят к задержкам согласования и потере документов</ColDesc>
						</Col1>
						<Col2>
							<ColTilte>Ускорить процесс</ColTilte>
							<ColDesc>При необходимости сокращения времени согласования заявки на командировку и формирования командировочных документов</ColDesc>
						</Col2>
						<Col3>
							<ColTilte> Сократить расходы</ColTilte>
							<ColDesc>Если при организации командировок могут возникнуть ошибки, приводящие к финансовым потерям.</ColDesc>
						</Col3>
					</Row>
			</Content2>
			<Content3>
					<TitleCenter>Комплексная автоматизация командировки</TitleCenter>
					<DescriptionCenter>Наибольшую эффективность на практике компаниям приносит комплексный подход в автоматизации командировки. Иными словами перенос целого бизнес-процесса в электронный формат — от заполнения заявки на командировку до оформления авансового отчета по командировочным расходам.</DescriptionCenter>
			</Content3>
			<Content>
					<ContentImg>
						<img src={Image2} alt="logo" />
					</ContentImg>
					<Info>
						<Title2>Лёгкий онлайн-контроль за расходами сотрудников</Title2>
						<Title2>Никаких долгих и сложных рутинных процессов</Title2>
						<Title2>Полная прозрачность прямо в вашем девайсе</Title2>
					</Info>
			</Content>
		</Container>
		<Modal show={modalVisible} fullscreen={false} onHide={() => setModalVisible(false)}>
      <Modal.Header closeButton>
        <Modal.Title>Войти</Modal.Title>
      </Modal.Header>
      <Modal.Body>
			<Form
        name="basic"
				{...layout}
        initialValues={{
          remember: true
        }}
        onFinish={handleSubmit}
      >
        <Form.Item
          label="Имя пользователя"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!'
            }
          ]}
        >
          <Input placeholder={russianText.login} prefix={<UserOutlined />} />
        </Form.Item>

        <Form.Item
          label="Пароль"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!'
            }
          ]}
        >
          <Input.Password
            placeholder={russianText.password}
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <SubmitButton type="primary" htmlType="submit" size="large">
            {russianText.submit}
          </SubmitButton>
        </Form.Item>
      </Form>
			</Modal.Body>
    </Modal>
		</div>
  )
}

HomeContainer.propTypes = {
}

export default HomeContainer
