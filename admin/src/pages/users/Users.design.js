import React from 'react'
import { Table, Button, Form, Modal, Input, Select, Avatar, DatePicker} from 'antd'
import { PlusOutlined, SearchOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const Users = ({
  users,
  addUserHandler
}) => {
  const [form] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchNickname, setSearchNickname] = React.useState(null)
  const [avatarUrl, setAvatarUrl] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }


  const [listUsers, setListUsers] = React.useState(null)

  React.useEffect(() => {
    const descriptiveUsers = users.map(user => {
      return {
				...user,
				fullname: `${user?.lastname} ${user?.firstname}`,
        genderName:
          (user.sex === 'male' && 'Мужской') ||
          (user.sex === 'female' && 'Женский')
      }
    })
    let searchArray = descriptiveUsers

    if (searchNickname) {
      searchArray = searchArray.filter(user => {
        return (
          user?.username?.toLowerCase().includes(searchNickname) ||
          user.lastname?.toLowerCase().includes(searchNickname) ||
          user.phoneNumber?.toLowerCase().includes(searchNickname) || 
					user.firstname?.toLowerCase().includes(searchNickname)
        )
      })
    }

    setListUsers(searchArray)
  }, [users, searchNickname])

  const columns = [
		{
      title: 'Аватарка',
      dataIndex: 'avatarUrl',
      render: item => {
        return <StyledImg src={item} alt="No Icon" />
      }
    },
    {
      title: 'Полное имя',
      dataIndex: 'fullname',
      width: '25%'
    },
    {
      title: 'Email',
      dataIndex: 'username',
      width: '15%'
    },
    {
      title: 'Номер телефона',
      dataIndex: 'phoneNumber',
      width: '15%'
    },
    {
      title: 'Пол',
      dataIndex: 'genderName',
      width: '10%'
    },
    {
      title: 'Действие',
      width: '20%',
      render: (text, item) => (
        <span>
          <Link to={`/users/${item.id}`}>Редактировать</Link>
        </span>
      )
    }
  ]


  const handleCreate = values => {
    console.log('values', values)
    addUserHandler(values)
    setModalVisible(false)
    setAvatarUrl(null)
  }

  return (
    <>
      <Table
        dataSource={listUsers}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Имя/Фамилие/Email/Номер телефона"
              onChange={e => {
                setSearchNickname(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> Новый пользователь
            </Button>
          </StyledHeaderContainer>
        )}
      />
      <Modal
        visible={modalVisible}
        title="Новый пользователь"
        okText="Создать"
        cancelText="Отмена"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.avatarUrl

              handleCreate({ ...values, avatarUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="username"
            label={getTooltip('Email пользователя', 'Email пользователя')}
            name="username"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Email пользователя`
              }
            ]}
          >
            <Input placeholder="Email" />
          </FormItem>
          <FormItem
            key="lastname"
            label={getTooltip('Фамилия пользователя', 'Фамилия пользователя')}
            name="lastname"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Фамилия пользователя`
              }
            ]}
          >
            <Input placeholder="Фамилия" />
          </FormItem>
          <FormItem
            key="firstname"
            label={getTooltip('Имя пользователя', 'Имя пользователя')}
            name="firstname"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Имя пользователя`
              }
            ]}
          >
            <Input placeholder="Имя" />
          </FormItem>
          <FormItem
            key="phoneNumber"
            label={getTooltip('Телефон пользователя', 'Телефон пользователя')}
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Телефон пользователя`
              }
            ]}
          >
            <Input placeholder="Телефон" />
          </FormItem>
          <FormItem
            key="gender"
            label={getTooltip(
              'Пол',
              'Пол пользователя'
            )}
            name="gender"
            rules={[
              {
                required: true,
                message: `Пожалуйста, выберите Пол пользователя`
              }
            ]}
          >
            <Select mode="single" placeholder="выберите Пол" showSearch>
              <Select.Option key="male" value="male">
                Мужской
              </Select.Option>
              <Select.Option key="female" value="female">
                Женский
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="birthday"
            label={getTooltip('Дата рождения пользователя', 'Дата рождения пользователя')}
            name="birthday"
            rules={[
              {
                required: true,
                message: `Пожалуйста, напишите Дата рождения пользователя`
              }
            ]}
          >
            <StyledDatePicker placeholder="Выберите дату" />
          </FormItem>
          <FormItem
            key="role"
            label={getTooltip(
              'Роль',
              'Роль пользователя'
            )}
            name="role"
            rules={[
              {
                required: true,
                message: `Пожалуйста, выберите Роль пользователя`
              }
            ]}
          >
            <Select mode="single" placeholder="выберите Роль" showSearch>
              <Select.Option key="admin" value="admin">
                Админ
              </Select.Option>
              <Select.Option key="user" value="user">
                Клиент
              </Select.Option>
              <Select.Option key="restaurantStaff" value="restaurantStaff">
                Персонал ресторана
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="avatarUrl"
            label={getTooltip('Аватарка', 'Аватарка пользователя')}
            name="avatarUrl"
            rules={[
              {
                required: false,
                message: `Пожалуйста, загрузите Аватарку`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setAvatarUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={avatarUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
    </>
  )
}

Users.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  addUserHandler: PropTypes.func.isRequired
}

export default Users
