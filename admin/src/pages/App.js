import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css'
import { ToastContainer } from 'react-toastify'
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  ApolloLink,
  concat
} from '@apollo/client'
import 'typeface-roboto'

import './App.css'
import './main.css'
import Login from './auth/Login'
import Home from './home/Home.container'

// head of department
import HeadOfDepartmentBuisnessProcessContainer from './headOfDepartment/HeadOfDepartmentBuisnessProcess.container'
import HeadOfDepartmentArchiveContainer from './headOfDepartment/HeadOfDepartmentArchive.container'

// employee
import EmployeeArchiveContainer  from './employee/EmployeeArchive.container'
import EmployeeBusinessTripContainer  from './employee/EmployeeBusinessTrip.container'
import PublicPage from './PublicPage'

// head
import HeadBusinessTripContainer from './head/HeadBusinessTrip.container'
import HeadArchiveContainer from './head/HeadArchive.container'

// hrManager
import HRManagerArchiveContainer from './hrManager/HRManagerArchive.container'
import HRManagerOrdersContainer from './hrManager/HRManagerOrders.container'

// accountant
import AccountantArchiveContainer  from './accountant/AccountantArchive.container'
import AccountantOrdersContainer from './accountant/AccountantOrders.container'

import withHelmet from '../hocs/withHelmet'
import { ProvideAuth } from '../context/useAuth'
import { ProvideLoading } from '../context/useLoading'
import LoadingDialog from '../context/LoadingDialog'
import PrivateRoute from '../hocs/PrivateRoute'

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_GRAPHQL_URL
})

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      authorization: localStorage.getItem('token')
        ? `Bearer ${localStorage.getItem('token')}`
        : null
    }
  })

  return forward(operation)
})

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink)
})

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <ProvideAuth>
        <ProvideLoading>
          <BrowserRouter>
            <ToastContainer />
            <LoadingDialog />
            <Route exact path="/public" component={PublicPage} />
            <Route exact path="/" component={PublicPage} />
            <Route exact path="/login" component={Login} />
            <Route
              exact
              path="/home"
              component={Home}
            />
            <PrivateRoute
              exact
              path="/headOfDepartmentBuisnessProcess"
              component={HeadOfDepartmentBuisnessProcessContainer}
              roles={['headOfDepartment']}
            />
            <PrivateRoute
              exact
              path="/headOfDepartmentArchive"
              component={HeadOfDepartmentArchiveContainer}
              roles={['headOfDepartment']}
            />
            <PrivateRoute
              exact
              path="/employeeBuisnessTrip"
              component={EmployeeBusinessTripContainer}
              roles={['employee']}
            />
            <PrivateRoute
              exact
              path="/employeeArchive"
              component={EmployeeArchiveContainer}
              roles={['employee']}
            />
            <PrivateRoute
              exact
              path="/headBuisnessTrip"
              component={HeadBusinessTripContainer}
              roles={['head']}
            />
            <PrivateRoute
              exact
              path="/headArchive"
              component={HeadArchiveContainer}
              roles={['head']}
            />
            <PrivateRoute
              exact
              path="/hrManagerArchive"
              component={HRManagerArchiveContainer}
              roles={['hrManager']}
            />
            <PrivateRoute
              exact
              path="/hrManagerArchive"
              component={HRManagerOrdersContainer}
              roles={['hrManager']}
            />
            <PrivateRoute
              exact
              path="/accountantArchive"
              component={AccountantArchiveContainer}
              roles={['accountant']}
            />
            <PrivateRoute
              exact
              path="/accountantOrders"
              component={AccountantOrdersContainer}
              roles={['accountant']}
            />
          </BrowserRouter>
        </ProvideLoading>
      </ProvideAuth>
    </ApolloProvider>
  )
}

const EnhancedApp = withHelmet([{ tag: 'title', content: 'Admin | Hero-app' }])(
  App
)

export default EnhancedApp
